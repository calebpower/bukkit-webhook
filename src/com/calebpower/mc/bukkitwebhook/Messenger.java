package com.calebpower.mc.bukkitwebhook;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Facilitates messages that are sent in-game or to the console.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public final class Messenger {

  private static String COLOR_PREFIX = ChatColor.BLUE + "[" + ChatColor.YELLOW + "WebHook" + ChatColor.BLUE + "] "; //prefix for in-game messages
  private static String PLAIN_PREFIX = "[WebHook] "; //prefix for console messages
  
  /**
   * The type of message that is being sent by the Messenger.
   * 
   * @author LordInateur
   */
  public static enum MessageType {
    NORMAL, ERROR, SUCCESS
  }

  /**
   * The color of the text being sent in a message by the Messenger.
   * 
   * @author LordInateur
   */
  public static enum MessageColor { //the color of the text being sent in a message
    NORMAL, ERROR, SUCCESS, EMPHASIZED
  }
  
  /**
   * Generates a NORMAL message.
   * For colored messages, surround text with splats (*) to emphasize text.
   * 
   * @param message the message to be sent
   * @param useColor <code>true</code> to use color or <code>false</code> to use no color
   * @return String representation of the formatted text
   */
  public static String generateMessage(String message, boolean useColor) {
    return generateMessage(message, MessageType.NORMAL, useColor);
  }
  
  /**
   * Generates a message.
   * For colored messages, surround text with splats (*) to emphasize text.
   * 
   * @param message the message to be sent
   * @param type the type of message to be sent
   * @param useColor <code>true</code> to use color or <code>false</code> to use no color
   * @return String representation of the formatted text
   */
  public static String generateMessage(String message, MessageType type, boolean useColor) {
    
    if(!useColor) return PLAIN_PREFIX + message; //use no colors if necessary
    
    ChatColor messageColor = null;
    
    switch(type) {
    case ERROR:
      messageColor = Config.getColor(MessageColor.ERROR);
      break;
    case SUCCESS:
      messageColor = Config.getColor(MessageColor.SUCCESS);
      break;
    case NORMAL:
    default:
      messageColor = Config.getColor(MessageColor.NORMAL);
      break;
    }
    
    String splitMessage[] = message.split("\\*"); //splats denote emphasized text (and act as toggles)
    String formattedMessage = new String();
    
    for(int i = 0; i < splitMessage.length; i++) {
      formattedMessage += splitMessage[i] + (i % 2 == 0 ? Config.getColor(MessageColor.EMPHASIZED) : messageColor);
    }
    
    return COLOR_PREFIX + messageColor + formattedMessage;
    
  }
  
  /**
   * Log a message to the Bukkit logger.
   * 
   * @param message the message to be logged
   */
  public static void log(String message) {
    log(Bukkit.getLogger(), message); //send unformatted messages directly to Bukkit console (Bukkit automatically takes care of prefix)
  }
  
  /**
   * Log a message to a specified logger.
   * 
   * @param logger the logger to send the message to
   * @param message the message to be logged
   */
  public static void log(Logger logger, String message) {
    logger.info(generateMessage(message, false)); //send messages to logger without color (but add the prefix)
  }
  
  /**
   * Send a message to a command sender.
   * 
   * @param commandSender the message target
   * @param message the message that should be sent
   */
  public static void sendMessage(CommandSender commandSender, String message) {
    commandSender.sendMessage(generateMessage(message, commandSender instanceof Player)); //send colorized messages to players only
  }
  
  /**
   * Send a message to a player.
   * 
   * @param player the message target
   * @param message the message that should be sent
   */
  public static void sendMessage(Player player, String message) {
    player.sendMessage(generateMessage(message, true)); //colorize messages that are sent to players
  }
  
}