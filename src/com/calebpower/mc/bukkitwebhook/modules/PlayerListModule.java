package com.calebpower.mc.bukkitwebhook.modules;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.calebpower.mc.bukkitwebhook.Messenger;

import spark.ModelAndView;
import spark.Request;
import spark.Response;

/**
 * List all players and determine the player count.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class PlayerListModule extends Module {

  /**
   * Default constructor to set the request type and route.
   */
  public PlayerListModule() {
    super(RequestType.GET_AND_POST, "/players");
  }

  @Override
  public ModelAndView action(Request request, Response response) {
    
    Messenger.log("User from *" + request.ip() + "* requested a list of all online players.");
    
    HashMap<String, Object> model = new HashMap<String, Object>();
    
    Collection<? extends Player> onlinePlayerList = Bukkit.getServer().getOnlinePlayers();
    model.put("count", onlinePlayerList.size()); //determine how many players are online
    
    String playerNames = "";
    for(Player player : onlinePlayerList) { //build JSON containing all online players
      playerNames += "\"" + player.getName() + "\",";
    }
    
    if(playerNames.substring(playerNames.length() - 1).equals(",")) playerNames = playerNames.substring(0, playerNames.length() - 1);
    model.put("names", playerNames);
    
    return new ModelAndView(model, "playerList.vtl");
    
  }

}
