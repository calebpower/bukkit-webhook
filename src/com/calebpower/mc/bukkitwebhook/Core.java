package com.calebpower.mc.bukkitwebhook;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.calebpower.mc.bukkitwebhook.modules.*;

/**
 * Core plugin driver; facilitates the configuration file and launches the
 * external interface. Provides for on-the-fly reloading from the console.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class Core extends JavaPlugin {
  
  public static final Module LOADED_MODULES[] = {
    new TestModule(),
    new PlayerListModule()
  };
  
  private ExternalInterface externalInterface = null;
  public File config = null;
  
  /**
   * On plugin enable, create the data folder and configuration file if
   * necessary. Then, launch the external interface.
   */
  @Override public void onEnable() {
    Messenger.log("Created by LordInateur. Loading plugin...");
    
    try {
      
      if(!getDataFolder().exists()) {
        Messenger.log("There appears to be no data folder. Creating data folder...");
        getDataFolder().mkdirs(); //create the plugin data directory if it doesn't exist
      }
      
      config = new File(getDataFolder(), "config.json");  //try to locate the configuration file
      
      if(!config.exists()) {
        Messenger.log("There appears to be no configuration file. Creating configuration file...");
        FileUtils.copyURLToFile(getClass().getResource("/config.json"), config); //generate the default configuration file if one doesn't exist 
      }
      
      Config.reload(config); //load the configuration file from the data folder

      externalInterface = new ExternalInterface(Config.getExternalPort()); //launch the external interface
      
    } catch(IOException e) {
      Messenger.log("An exception was caught when attempting to load the contents of the data folder.");
    }

  }
  
  /**
   * On disable, kill the external interface.
   */
  @Override public void onDisable() {
    Messenger.log("Disabling plugin...");
    externalInterface.kill(); //kill the external interface
  }
  
  /**
   * Respond appropriately to a user- or console-issued command.
   */
  @Override public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {

    if(args.length == 1 && args[0].equalsIgnoreCase("port")) WHCommand.getActivePort(sender);
    else if(args.length == 1 && args[0].equalsIgnoreCase("reload")) WHCommand.reload(config, sender); //reloads configuration file
    else WHCommand.remindOfUsage(sender); //remind sender of usage if no other commands could be completed
    
    return true;
    
  }
  
}
