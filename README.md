# Bukkit WebHook
### Seamless Bukkit + Website Integration

---

This project requires two pieces in order to function as intended:

1. The Bukkit plugin, properly installed in the Bukkit plugins folder
2. A web application or script that should utilize the WebHook

For now, the API is hard-coded to run on port **3456**; a configuration file shall be added later.

To test the plugin without a web application, a program such as **curl** should be used. The following example assumes that you are running **curl** on the same server as the Bukkit implementation.
After execution, note that there exists both a JSON response and a new message in the Bukkit console log.

```bash
$ curl --data "" 127.0.0.1:3456/test # sends empty POST request to /test resource
```

Response:

```json
{ "response": "ok" }
```


Here's an example of how one might use the API to contact the server. *NOTE:* this particular file may not run in modern browsers if the minecraft server and the example HTML file are on the same machine for security reasons. Windoze users, use Internet Explorer if necessary (shudder).


```html
<!doctype html>
<html>
  <head>
    <title>Test</title>
    <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
    <script>

      $(document).ready(function() { //enable link action
        document.getElementById('dasLink').addEventListener('click', function() {
          testTheThang();
        });
      });

      function testTheThang() { //send the message (drawn-out example = hooray for education)
        var http = new XMLHttpRequest();
        var url = "http://127.0.0.1:3456/test";
        http.open("POST", url, true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() { //wait for magical response and stick it into response div
          if(http.readyState == 4 && http.status == 200) { //but only if the server likes us
            document.getElementById('dasResponse').innerHTML = http.responseText;
          }
        }

        http.send(""); //parms go here (but /test resource doesn't require params)
      }
    </script>
  </head>
  <body>
    <a id='dasLink' href='javascript:void(0);'>DO THE THANG</a><br /><br /> <!-- click me pls -->
    Response:<br />
    <div id='dasResponse'>[ no response yet ]</div> <!-- response magically appears here -->
  </body>
</html>
```

Note: Obviously, this is pre-alpha code. Currently there is little to no functionality, and what functionality there is bound to be be buggy. I will do my best to ensure that all pushes to the *master* branch are buildable.

This readme was last amended on July 4, 2017 in the wee hours of the morning. Woo 'merica.
I'm releasing this version of the Bukkit WebHook under the Apache License v2.0. (c) Caleb L. Power.