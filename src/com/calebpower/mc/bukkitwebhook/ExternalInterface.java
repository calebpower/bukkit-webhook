package com.calebpower.mc.bukkitwebhook;

import spark.Spark;

import static spark.Spark.*;

import com.calebpower.mc.bukkitwebhook.modules.Module;
import com.calebpower.mc.bukkitwebhook.modules.Module.RequestType;

/**
 * Launches and configures the external RESTful API
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class ExternalInterface {
  
  private static ExternalInterface activeExternalInterface = null; //keep track of the active External Interface

  private int externalPort = -1; //keep track of the active external port
  
  /**
   * Open the External Interface on a specific external port
   * 
   * @param externalPort the port that should be used to open the External Interface
   */
  public ExternalInterface(int externalPort) {
    if(activeExternalInterface != null) { //(for live reloading) if it exists, close the pre-existing and open external port
      activeExternalInterface.kill();
    }
    
    activeExternalInterface = this; //make sure to keep track of the External Interface that is currently live
    this.externalPort = externalPort;
    Messenger.log("Opening external port " + externalPort);
    
    port(externalPort); //actually open the port
    Spark.staticFiles.location("/"); //static files should be in the root of the jar file
    
    for(Module module : Core.LOADED_MODULES) {
      
      if(module.getRequestType() == RequestType.GET_ONLY || module.getRequestType() == RequestType.GET_AND_POST) {
        Messenger.log("Setting GET route for " + module.getRoute());
        get(module.getRoute(), module::action, new VelocityTemplateEngine());
      }
      
      if(module.getRequestType() == RequestType.POST_ONLY || module.getRequestType() == RequestType.GET_AND_POST) {
        Messenger.log("Setting POST route for " + module.getRoute());
        post(module.getRoute(), module::action, new VelocityTemplateEngine());
      }
      
    }
    
  }
  
  /**
   * Retrieves the active external interface.
   * 
   * @return ExternalInterface the active external interface
   */
  public static ExternalInterface getActiveExternalInterface() {
    return activeExternalInterface;
  }
  
  /**
   * Retrieves the active external port.
   * 
   * @return int representation of the active external port
   */
  public int getPort() {
    return externalPort;
  }
  
  /**
   * Kills the current External Interface.
   */
  public void kill() {
    stop();
    Messenger.log("External port " + externalPort + " has been unbound.");
  }
  
}
