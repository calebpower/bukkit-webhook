package com.calebpower.mc.bukkitwebhook;

import java.io.File;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Executes all in-game and in-console commands.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class WHCommand {
  
  /**
   * Executes command to retrieve the active port of the external interface and
   * sends the response to the original command sender.
   * 
   * @param sender the sender that the response should be sent to
   */
  public static void getActivePort(CommandSender sender) {
    Messenger.sendMessage(sender, "Active port: *" + ExternalInterface.getActiveExternalInterface().getPort() + "*");
  }
  
  /**
   * Executes command to reload the configuration file and sends the response
   * toi the original command sender.
   * 
   * @param config the configuration file that should be reloaded
   * @param sender the sender that the response should be sent to
   */
  public static void reload(File config, CommandSender sender) {
    Config.reload(config);
    Messenger.sendMessage(sender, "The configuration file has been reloaded.");
    if(sender instanceof Player) Messenger.log("The configuration file was reloaded by " + ((Player)sender).getName());
  }
  
  /**
   * Reminds the command sender of the WebHook syntax.
   * 
   * @param sender the sender that the response should be sent to
   */
  public static void remindOfUsage(CommandSender sender) {
    Messenger.sendMessage(sender, "Usage:");
    Messenger.sendMessage(sender, "/webhook port");
    Messenger.sendMessage(sender, "/webhook reload");
  }
  
}
