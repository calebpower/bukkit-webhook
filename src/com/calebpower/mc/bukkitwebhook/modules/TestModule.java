package com.calebpower.mc.bukkitwebhook.modules;

import java.util.HashMap;

import com.calebpower.mc.bukkitwebhook.Messenger;

import spark.ModelAndView;
import spark.Request;
import spark.Response;

/**
 * Test module that simply returns a response and logs an action in the console.
 * 
 * @author LordInateur (Caleb L. Power)
 *
 */
public class TestModule extends Module {

  /**
   * Default constructor to set the request type and route.
   */
  public TestModule() {
    super(RequestType.GET_AND_POST, "/test");
  }

  @Override
  public ModelAndView action(Request request, Response response) {
    
    Messenger.log("The connection at *" + request.ip() + "* has utilized the RESTful API."); //log a message for testing purposes
    return new ModelAndView(new HashMap<String, Object>(), "testResponse.vtl"); //return a generic HTTP success response
    
  }

}
