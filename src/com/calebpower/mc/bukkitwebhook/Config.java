package com.calebpower.mc.bukkitwebhook;

import java.io.File;
import java.util.Scanner;

import org.bukkit.ChatColor;
import org.json.JSONObject;

import com.calebpower.mc.bukkitwebhook.Messenger.MessageColor;

/**
 * Reads and parses from a give configuration file.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class Config {
  
  private static int externalPort = 3456;
  private static ChatColor errorChatColor = ChatColor.RED;
  private static ChatColor successChatColor = ChatColor.GREEN;
  private static ChatColor normalChatColor = ChatColor.WHITE;
  private static ChatColor emphasizedChatColor = ChatColor.YELLOW;
  
  /**
   * Loads or reloads a configuration file on the fly
   * 
   * @param file the configuration file
   * @return <code>true</code> if the program was at least partially successful
   *         in loading attributes from the configuration file and <false> if
   *         the configuration file could not be read at all
   */
  public static boolean reload(File file) {
    
    if(!file.canRead() || !file.isFile()) return false; //if the file exists, do not attempt to do anything further
    
    String raw = new String();
    
    try {
      Scanner scanner = new Scanner(file); //attempt to open the configuration file
      while(scanner.hasNext()) raw += scanner.nextLine(); //read the entire physical file in one go
      scanner.close(); //close off the resource
    } catch(Exception e) {
      return false;
    }
    
    JSONObject json = new JSONObject(raw); //convert the raw data into a JSON object
    if(json.has("externalPort")) externalPort = json.getInt("externalPort"); //port that the external interface should open on
    if(json.has("chatColors")) {
      if(json.getJSONObject("chatColors").has("error"))
        errorChatColor = resolveColor(json.getJSONObject("chatColors").getString("error").toUpperCase()); //error messages
      if(json.getJSONObject("chatColors").has("success"))
        successChatColor = resolveColor(json.getJSONObject("chatColors").getString("success").toUpperCase()); //success messages
      if(json.getJSONObject("chatColors").has("normal"))
        normalChatColor = resolveColor(json.getJSONObject("chatColors").getString("normal").toUpperCase()); //normal messages
      if(json.getJSONObject("chatColors").has("emphasized"))
        emphasizedChatColor = resolveColor(json.getJSONObject("chatColors").getString("emphasized").toUpperCase()); //emphasized text within messages
    }
    
    return true;
    
  }
  
  /**
   * Retrieve the custom external port
   * 
   * @return int representation of the desired external port
   */
  public static int getExternalPort() {
    return externalPort;
  }
  
  /**
   * Retrieves the configured color for particular message text types.
   * This is to be used externally when retrieving configured values.
   * 
   * @param the abstract name of desired color
   * @return ChatColor denoting the desired color of the text
   */
  public static ChatColor getColor(MessageColor color) {
    
    switch(color) {
    case EMPHASIZED:
      return emphasizedChatColor;
    case ERROR:
      return errorChatColor;
    case SUCCESS:
      return successChatColor;
    case NORMAL:
    default:
      return normalChatColor;
    }
    
  }
  
  /**
   * Resolve the string representations of the colors defined in the config file.
   * This is to be used internally when resolving user-defined values in the config file.
   * 
   * @param color the string representation of the desired color
   * @return ChatColor denoting the resolved color
   */
  private static ChatColor resolveColor(String color) {
    
    switch(color) {
    case "BLACK":
      return ChatColor.BLACK;
    case "DARK_BLUE":
      return ChatColor.DARK_BLUE;
    case "DARK_GREEN":
      return ChatColor.DARK_GREEN;
    case "DARK_AQUA":
      return ChatColor.DARK_AQUA;
    case "DARK_RED":
      return ChatColor.DARK_RED;
    case "DARK_PURPLE":
      return ChatColor.DARK_PURPLE;
    case "GOLD":
      return ChatColor.GOLD;
    case "GRAY":
      return ChatColor.GRAY;
    case "DARK_GRAY":
      return ChatColor.DARK_GRAY;
    case "BLUE":
      return ChatColor.BLUE;
    case "GREEN":
      return ChatColor.GREEN;
    case "AQUA":
      return ChatColor.AQUA;
    case "RED":
      return ChatColor.RED;
    case "LIGHT_PURPLE":
      return ChatColor.DARK_PURPLE;
    case "YELLOW":
      return ChatColor.YELLOW;
    case "WHITE":
    default:
      return ChatColor.WHITE; //white will be the default for colors that couldn't be parsed
    }
  }
  
  

}
